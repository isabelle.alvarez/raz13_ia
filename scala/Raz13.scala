package viabilitree.approximation.example.raz13
/*
 * Copyright (C) 10/10/13 Isabelle Alvarez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import viabilitree.model._
import math._

case class Raz13(
  timeStep: Double = 0.1,
  N: Double = 1, //M de Sophie
  DeltaT: Double = 1,
  A: Double = 0.392089377298999, // A
  rho1min: Double = 0.00374956833815908, // P(1,1,1,1,1) l'ex A2_0
  rho1max: Double = 0.703874267662894, // voir fichier paramètres
  Tm: Double = 8.0, //demi-vie adaptation en année
  b: Double = 1933 / 1000.0, // k€/y^-1 mean welfare ratio
  c_td: Double = 31.98083 / 1000.0, // k€, infinitesimal cost of targeted communication policy
  c_pc: Double = 70.14964 / 1000.0, //  k€, infinitesimal cost of global communication policy
  Umax_pc: Double = 0.4561677, // 0.1314134, // rho1bar_pc max possible atteignable par le contrôle
  Umax_td: Double = 0.1699388, //0.08167006, // rho1bar_td max possible atteignable par le contrôle
  v_m: Double = 1.0, // en m , size of flood for which half the effect of instant gain of adaptation
  // is reached
  max_s: Double = 0.2721925, // max(rho1_bar_v_base) to adjust to Sophie min of rho1_bar_v
  min_s: Double = 0.02426212, //min(rho1_bar_v_base), to adjust to Sophie max of rho1_bar_v
  com_type: String = "td" //type of communication policy
) {

  /* PARAMETRES
  N (M  at Sophie's is the probability that somebody willing to adapt is going to adapt)
  DeltaT is pas de temps de la dynamique multi-agent
  A coeff de normalisation de rho (exprimé comme fonction linéaire des P(a,5)). Donc coeff de alpha
  rho1_min : coefficient minimal de variation de alpha (quand alpha =0) (et valeur minimale de rho)
  Tm is T_bar : half-life of adaptation
  M flood size for which impact is half the max (1/2)
  A3 is suppose to be 1 but with concordance with Sophie it is A3= 1-mean(diff_is)
  A2 and U (control) must be such that A2/4 * U > A1, otherwise control is inefficient.
  By default A1=Q+delta with Q=log(2)/Tm, and delta is the correction to apply to take into account the slow fall of attributes

   */
  /* ON NE PEUT PAS FAIRE COMME CA
  def dynamic2(state: Vector[Double], control: Vector[Double]) = {
    val A2bis = min((1-A2)/2,0.2)
    // A2bis dans ]0,1-A2[
    def alphaDot(state: Vector[Double], t: Double) =
      - A1 * state(0) + (A2 + A2bis * state(0)) * state(0) * (1-state(0))
    def wDot(state: Vector[Double], t: Double) = b - C * control(0)

    val dynamic = Dynamic(alphaDot, wDot)
    dynamic.integrate(state.toArray, integrationStep, timeStep)
  }

def dynamic(state: Vector[Double], control: Vector[Double]) = {
  dynamic1(state,control)
}
*/
  def kilo: Boolean = true
  // règle le pb de l'affichage des € en k€ dans la fonction dommage
  // si false il faut changer les valeurs dans l'entête
  //

  def integrationStep: Double = timeStep / 10.0
  //  def vMax = 5.0
  def Q = log(2) / Tm
  def A1: Double = Q
  def A2: Double = A
  // A1 peut valoir en fait ln(2)/TM, ie en TM alpha aura perdu la moitié de sa valeur initiale
  def C: Double = if (com_type == "td") c_td else c_pc
  def A3: Double = 0.9842128 // # A3= 1-mean(result_alpha0_i$alpha_T_i - result_alpha0_s$alpha_T_s)
  // diminishes the delta in case of flood to anticipate the extra diminishing dynamics of alpha after flood
  def beta_v(s: Double): Double = {
    val beta_100: Double = 1 - 0.366
    val beta_250: Double = 1 - 0.0332
    if (s < 100) { beta_100 * s / 100 }
    else {
      if (s < 250) {
        beta_100 + (s - 100) * (beta_250 - beta_100) / (250 - 100)
      } else {
        min(1, beta_250 + (s - 250) / 1000)
      }
    }
  } //fin de beta_v

  // TODO coût du contrôle en fonction du type de contrôle

  def dynamic(state: Vector[Double], control: Vector[Double]) = {
    def alphaDot(state: Vector[Double], t: Double) =
      1 / N * 1 / DeltaT * (-A1 * state(0) + (A2 * state(0) + 1) * (1 - state(0)) * (control(0) + rho1min))
    def wDot(state: Vector[Double], t: Double) = b - C * control(0)

    val dynamic = Dynamic(alphaDot, wDot)
    dynamic.integrate(state.toArray, integrationStep, timeStep)
  }

  // real damage function from the wiki file
  // see at the end of the file

  // filename for damages
  def lectureDamage = {
    import better.files.File
    val file = File("images/DataDamage.csv")
    val lines = file.lines.drop(1).toVector
    val tempVMIN = lines.map(p => p.split(";").toList(0).toDouble)
    val tempVMAX = lines.map(p => p.split(";").toList(1).toDouble)
    val coeff1 = lines.map(p => p.split(";").toList(4).toDouble)
    val min1 = lines.map(p => p.split(";").toList(5).toDouble)
    val coeff2 = lines.map(p => p.split(";").toList(6).toDouble)
    val min2 = lines.map(p => p.split(";").toList(7).toDouble)
    (tempVMIN, tempVMAX, coeff1, min1, coeff2, min2)
  }

  val (tempVMIN, tempVMAX, coeff1, min1, coeff2, min2) = lectureDamage
  /*
  println ("tempVMIN " + tempVMIN)
  println ("tempVMAX " + tempVMAX)
  println ("coeff1 " + coeff1)
  println ("min1 " + min1)
  println ("coeff2 " + coeff2)
  println ("min2 " + min2)
*/

  def d_2real(alpha: Double, s: Double): Double = { // en k€ ; s en m ; avec protection
    val ind = tempVMAX.indexWhere(_ > 100 * s)

    val coeff = ind match {
      case -1 => coeff2.last
      case x => coeff2(x)
    }
    val min = ind match {
      case -1 => min2.last
      case x => min2(x)
    }
    val smin = ind match {
      case -1 => tempVMIN.last
      case x => tempVMIN(x)
    }

    val d2final = (((100 * s) - smin) * coeff + min) / 1000.0
    //   println( "alpha s " + alpha + "  " +s + " with " +  "ind " + ind + " / coeff " + coeff +  " smin " + smin+ "  => d2: " +d2final)
    d2final
  }

  def d_1real(alpha: Double, s: Double): Double = { // en k€ ; s en m ; sans protection
    val ind = tempVMAX.indexWhere(_ > 100.0 * s)

    val coeff = ind match {
      case -1 => coeff1.last
      case x => coeff1(x)
    }
    val min = ind match {
      case -1 => min1.last
      case x => min1(x)
    }
    val smin = ind match {
      case -1 => tempVMIN.last
      case x => tempVMIN(x)
    }

    val d1final = (((100 * s) - smin) * coeff + min) / 1000.0
    //    println( "alpha s " + alpha + "  " +s + " with " +  "ind " + ind + " / coeff " + coeff +  " smin " + smin+ "  => d1: " +d1final)
    d1final
  }

  def damage_base(alpha: Double, s: Double): Double = { // s en mètre
    val le_damage: Double = ((1 - alpha) * d_1real(alpha, s) + alpha * d_2real(alpha, s)) //en k€ si kilo = true
    if (kilo) {
      le_damage
    } else {
      le_damage * 1000.0
    }
  }

  def damage(alpha: Double, s: Double): Double = { // k€ from D.30 ; s en mètre
    // scenario with
    1.0 / 3.0 * damage_base(alpha, s) + 1.0 / 3.0 * damage_base(alpha, max(s - 0.5, 0)) + 1.0 / 3.0 * damage_base(alpha, max(s - 1.0, 0))
  }

  def beta_v(s1: Double, sc: String = "sc1"): Double = { // can be changed with Katrin population height distribution scenario
    val s = 100 * s1 // on attend des cm
    if (sc == "sc1") {
      // proportion des individus touchés par une inondation de taille s en cm
      //  # regression à partir de :
      //  # v=0 beta_v=0
      //  # v=100 beta_v = 1-0.366
      //  # v=250 beta_v = 1-0.0332 puis linéaire 0.1 pour 1 m au-dessus de 250cm, i.e. 284 -> 1
      val beta_100: Double = 1 - 0.366
      val beta_250: Double = 1 - 0.0332
      if (s < 100.0) beta_100 * s / 100.0 else {
        if (s < 250.0) beta_100 + (s - 100) * (beta_250 - beta_100) / (250.0 - 100.0) else min(1, beta_250 + (s - 250) / 100.0)
      }
    } else 1 // default value 1 every one is impacted
  }

  def perturbation(state: Vector[Double], s: Double) = {
    def alphaDelta(state: Vector[Double], s: Double) = A3 * (1 - state(0)) * beta_v(s) * (min_s + (max_s - min_s) * s * s / (v_m * v_m + s * s))
    def wDelta(state: Vector[Double], s: Double) = -damage(state(0), s)
    (alphaDelta(state, s), wDelta(state, s))
  }

  def jump(state: Vector[Double], s: Double) = {
    val (alphaDelta, wDelta) = perturbation(state, s)
    Vector(state(0) + alphaDelta, state(1) + wDelta)
  }
  // pour avoir un vecteur en sortie
  /*
  def jump(state:Vector[Double], s: Double) = {
    val (alphaDelta,wDelta) = perturbation(state,s)
    (state(0) + alphaDelta, state(1) + wDelta )
  }
*/

  // On a besoin d'une soft_appartenance à un noyau qui tienne compte de la manière dont on sort de l'ensemble
  // TODO fix pb when some states are outside wLim (ex. v=3) outOfMemoryError
  // happens when jumpV is a reverse perturbation

  // true if the present point is in the kernel and if its image by the perturbation is also in the kernel OR if it is out the upper limit
  def softJump(state: Vector[Double], jumpV: Vector[Double] => Vector[Double],
    viableSet: viabilitree.kdtree.Tree[viabilitree.viability.kernel.KernelContent],
    viabProblem: viabilitree.viability.kernel.KernelComputation): Boolean = {
    val jumpState = jumpV(state)
    val zoneLim = viabProblem.zone
    val wLim = zoneLim.region(1).max
    (viableSet.contains(viabilitree.viability.kernel.KernelContent.label.get, state) &&
      (viableSet.contains(viabilitree.viability.kernel.KernelContent.label.get, jumpState)) ||
      jumpState(1) >= wLim)
  }

  // ici c'est différent il faut être pragmatique
  // inverse i.e. valeurs desquelles on est parti avant la perturbation s
  def inverseJumpDirect(state: Vector[Double], s: Double) = {
    val (alphaDirect, wDirect) = inversePerturbation(state, s)
    Vector(alphaDirect, wDirect)
  }

  def inversePerturbation(state: Vector[Double], s: Double) = {
    val Aa = A3 * beta_v(s) * (min_s + (max_s - min_s) * s * s / (v_m * v_m + s * s))
    def alphaDirect(state: Vector[Double], s: Double) = (state(0) - Aa) / (1 - Aa)
    def wDirect(state: Vector[Double], s: Double) = state(1) + damage(alphaDirect(state, s), s)
    (alphaDirect(state, s), wDirect(state, s))
  }

  // true if the present point is the image of a kernel point by a perturbation (jumpV)
  def softInverseJump(state: Vector[Double], jumpV: Vector[Double] => Vector[Double],
    viableSet: viabilitree.kdtree.Tree[viabilitree.viability.kernel.KernelContent],
    viabProblem: viabilitree.viability.kernel.KernelComputation): Boolean = {
    val jumpState = jumpV(state)
    (viableSet.contains(viabilitree.viability.kernel.KernelContent.label.get, jumpState))
  }

}

