# RAZ13_ia

Data and code files for indicator 2 in article Very diverse indicators for dealing with uncertainties of coming negative event.
## Summary
This directory contains the json data file which is used by the .r file to produce the indicator 2 maps, such as the example figure in the paper.

The .R file contains the script to run in RStudio or directly in R console to produce the figure for indicator 2 in the paper.

The .jason file contains all the data necessary to work with indicator 2 with parameters specified in the paper and reminded in this file. It is the output of the viability computation done with viabbilitree from the scala files.

The .scala files contains the specific code that was used to run viabilitree (the viability software used in this project) to produce the .json file. These files are to be imported in viabilitree as a new example then run. The execution output is the .jason file above.

The DataDamage.csv file (in viabilitree/images) contains the damage data used by all indicators.

## viability library to run the scala files
https://github.com/ISCPIF/viabilitree

Viabilitree is free to download, midify and run.
Viabilitree has to be installed locally, for example with IntelliJ IDEA with plugin Scala, or other IDE.
The .scala files should to be added as a new project to viabilitree, or should replace all existing files in the present RAZ13 project in Viabilitree.

## R library to execute .R file
https://cran.r-project.org/

